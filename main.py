import json
from fastapi import FastAPI, Request, Response, HTTPException

from helpers.validators import ClientRequestPaymentPayload
from services.payment_process import process_client_request


app = FastAPI()


@app.get("/")
def read_root():
    return {"hello":"world"}

@app.post("/payment", status_code=201)
def create_payment_request(payment_info:ClientRequestPaymentPayload, request: Request, response: Response):
    payment = process_client_request(payment_info, request)
    json_data = json.loads(payment.text)

    if payment.status_code != 201:
        print({f'--> errors requesting to create to tpaga. Code: {json_data["error_code"]} message: {json_data["error_message"]}'})
        raise HTTPException(status_code=payment.status_code, detail='there was error in payment process')

    return json_data
