import base64
import datetime
import hashlib
import os
import pytz


TPAGA_USR=os.environ.get('TPAGA_USR')
TPAGA_PWD=os.environ.get('TPAGA_PWD')
EXPIRATION_TIME=os.environ.get('EXPIRATION_TIME')

def get_auth():
    auth_string=f'{TPAGA_USR}:{TPAGA_PWD}'
    auth_encode=auth_string.encode('ascii')
    auth_encode=base64.b64encode(auth_encode)
    auth_encode=auth_encode.decode('ascii')
    return auth_encode

def get_order_id():
    return 'BILLCODEAUTOGENERATED'

def get_purchase_detail_url():
    return 'https://merchanthost/purchase-finalization-url'

def get_idempotency_token(user_id: str, order_id: str) -> str:
    return hashlib.sha1((f'{user_id}:{order_id}').encode('utf-8')).hexdigest()

def get_expiration_date() -> str:
    tz = pytz.timezone("America/Bogota")
    unaware_now = datetime.datetime.now()
    hours_added = datetime.timedelta(hours=int(EXPIRATION_TIME))
    unaware_expiration = unaware_now + hours_added
    aware_dt = tz.localize(unaware_expiration)
    expiration = aware_dt.isoformat(timespec='milliseconds')
    return expiration
