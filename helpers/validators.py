from pydantic import BaseModel
from typing import List, Optional


class PurchaseItem(BaseModel):
    name: str
    value: int

class TpagaPaymentPayload(BaseModel):
    cost: int
    purchase_details_url: str
    voucher_url: str
    idempotency_token: str
    order_id: str
    terminal_id: str
    purchase_description: str
    purchase_items: List[PurchaseItem] = None
    user_ip_address: str
    expires_at: str

class ClientRequestPaymentPayload(BaseModel):
    user_id: str
    cost: int 
    terminal_id: str
    purchase_description: str
    purchase_items: List[PurchaseItem] = None
    user_ip_address: str
