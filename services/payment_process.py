import json
import os
import requests

from fastapi import Request, HTTPException
from functools import reduce

from helpers.purchase_helper import get_auth, get_expiration_date, get_idempotency_token, get_order_id, get_purchase_detail_url 
from helpers.validators import ClientRequestPaymentPayload, TpagaPaymentPayload 


TPAGA_API_URL=os.environ.get('TPAGA_API_URL')


def process_client_request(payment_info: ClientRequestPaymentPayload, request: Request) -> dict:
    
    if payment_info.purchase_items:
        total_price = reduce((lambda x, y: x.value+y.value), payment_info.purchase_items)
    
    if payment_info.purchase_items and total_price != payment_info.cost:
        raise HTTPException(status_code=409, detail="cost and total value doesn't match")

    voucher_url = 'link to purchase detail'
    purchase_detail_url = get_purchase_detail_url()
    order_id = get_order_id()
    idempotency_token = get_idempotency_token(payment_info.user_id, order_id)
    description = ''

    if payment_info.purchase_items:
        description = ' \n'.join([f'{item.name}: ${item.value}' for item in payment_info.purchase_items])

    purchase_decription = f'Your purchase: \n{description}'
    user_ip_address = request.client.host
    expires_at = get_expiration_date()

    tpaga_payment_request_payload = TpagaPaymentPayload(
        cost=payment_info.cost,
        voucher_url=voucher_url,
        purchase_details_url=purchase_detail_url,
        idempotency_token=idempotency_token,
        order_id=order_id,
        purchase_description=purchase_decription,
        user_ip_address=user_ip_address,
        purchase_items=payment_info.purchase_items,
        expires_at=expires_at,
        terminal_id=payment_info.terminal_id
    )

   
    return send_tpaga_payment_request(tpaga_payment_request_payload)


def send_tpaga_payment_request(data: TpagaPaymentPayload) -> dict:
   
    headers = {
        'Authorization': f'Basic {get_auth()}',
        'Cache-Control': 'no-cache',
        'Content-Type': 'application/json'
    }
    try:
        response = requests.post(f'{TPAGA_API_URL}/create', headers=headers, json=data.dict())
    except Exception as e:
        print(f'--> error requesting tpaga API {e}')
        raise HTTPException(status_code=502, detail="can't connect to tpaga API")

    return response
